/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tweetscollector;

import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import tweetscollector.store.StoreManager;
import tweetscollector.trends.TrendsUpdateListener;
import twitter4j.ConnectionLifeCycleListener;
import twitter4j.FilterQuery;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

/**
 *
 * @author intelWorX
 */
public class FilterService implements TrendsUpdateListener {

    //protected ArrayList<String> trendingTopics;
    //protected FilterQuery filterQuery;
    protected TwitterStream twitterStream;
    protected TweetReceiver receiver;
    //protected int runs = 0;
    protected static boolean everRun = false;
    static final protected long CONNECT_DISCONNECT_WAIT = 5000;
    private static FilterService instance;
    //private boolean connected = false;

    private FilterService() {
        receiver = new TweetReceiver();
        Configuration config = new ConfigurationBuilder()
                .setGZIPEnabled(false)
                .build();
        twitterStream = new TwitterStreamFactory(config).getInstance();

        twitterStream.addListener(receiver);
        if (twitterStream.getConfiguration().isGZIPEnabled()) {
            System.out.println("GZip Enabled");
        } else {
            System.out.println("GZip Disabled");
        }
        twitterStream.addConnectionLifeCycleListener(new OtherStreamConnected());
    }

    private void runFilter(String[] trendingTopics) {
        final FilterQuery filterQuery = new FilterQuery();
        filterQuery.language(Configs.LANG)
                .locations(Configs.BOUNDING_BOXES)
                .track(trendingTopics);
        
        twitterStream.filter(filterQuery);
        System.out.println("Should start filtering... " + filterQuery);
    }

    @Override
    public void trendsUpdated() {
        if (everRun) {
            twitterStream.cleanUp();
            twitterStream.shutdown();
        } else {
            update();
            System.out.println("Never ran");
            everRun = true;
        }
    }

    //@Override
    public void update() {
        Set<String> trendingTopicList = StoreManager.getInstance().getTrendingTopics();
        int size = trendingTopicList.size() < Configs.TRACK_MAX ? trendingTopicList.size() : Configs.TRACK_MAX;

//      String topics[] = new String[everRun ? size + 1 : size];
        String topics[] = new String[size];
        int idx = 0;
        for (String topic : trendingTopicList) {
            topics[idx++] = topic;
            if (idx >= size) {
                break;
            }
        }
        
        System.out.println("Rerunning filter for [" + size + "] topics");
        runFilter(topics);
    }

    public static FilterService getInstance() {
        if (instance == null) {
            instance = new FilterService();
        }
        return instance;
    }

    public static class OtherStreamConnected implements ConnectionLifeCycleListener {

//        private final TwitterStream otherStream;
//        private final int otherStreamId;
        private final int myStreamId = 1;

        public OtherStreamConnected() {
        }

        @Override
        public void onConnect() {
//            if (otherStream != null) {
//                System.out.println("Shutting down other stream: " + otherStreamId);
//                otherStream.clearListeners();
//                otherStream.shutdown();
//            }
            //instance.connected = true;
            //instance.disconnected = false;
            System.out.println("Connected");
        }

        @Override
        public void onDisconnect() {
            System.out.println(myStreamId + " is Disconnected!");
        }

        @Override
        public void onCleanUp() {
            System.out.println("Cleaning up... and starting new one");
            instance = new FilterService();
            instance.update();
        }

    }

}
