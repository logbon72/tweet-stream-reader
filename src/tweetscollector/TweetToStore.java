/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tweetscollector;

import org.json.JSONArray;
import org.json.JSONObject;
import twitter4j.Status;
import twitter4j.URLEntity;

/**
 *
 * @author intelWorX
 */
public class TweetToStore {

    protected String text;
    protected long id;
    protected String[] links;
    protected int favouriteCount;
    protected int retweetCount;
    protected boolean isRetweet;

    public TweetToStore(Status tweetStatus) {
        text = tweetStatus.getText();
        id = tweetStatus.getId();
        favouriteCount = tweetStatus.getFavoriteCount();
        retweetCount = tweetStatus.getRetweetCount();
        isRetweet = tweetStatus.isRetweet();
        if (tweetStatus.getURLEntities().length > 0) {
            links = new String[tweetStatus.getURLEntities().length];
            int idx = 0;
            for (URLEntity url : tweetStatus.getURLEntities()) {
                links[idx++] = url.getURL();
            }
        }

    }

    public String getJSONString() {
        JSONObject rep = new JSONObject();
        rep.put("text", text);
        rep.put("id", id);
        rep.put("favourites", favouriteCount);
        rep.put("is_retweet", isRetweet);
        rep.put("retweets", retweetCount);
        JSONArray linksArr = new JSONArray();
        if(links != null){
            for(String url : links){
                linksArr.put(url);
            }
        }
        rep.put("links", linksArr);
        return rep.toString();
    }

    public int getFavouriteCount() {
        return favouriteCount;
    }

    public long getId() {
        return id;
    }

    public String[] getLinks() {
        return links;
    }

    public int getRetweetCount() {
        return retweetCount;
    }

    public String getText() {
        return text;
    }
    
    
}
