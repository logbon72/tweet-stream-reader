/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tweetscollector;

import java.io.IOException;
import tweetscollector.store.StoreManager;
import twitter4j.TwitterException;

/**
 *
 * @author intelWorX
 */
public class TweetsCollector {

    /**
     * @param args the command line arguments
     * @throws twitter4j.TwitterException
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws TwitterException, IOException {

        TrendingTopicsUpdater topicsUpdater = TrendingTopicsUpdater.getInstance();
        FilterService filterService = FilterService.getInstance();
        topicsUpdater.addUpdateListener(filterService);

        
        
        Runtime.getRuntime().addShutdownHook(new Thread(){
            @Override
            public void run() {
                StoreManager.getInstance().saveAll();
            }
        });
        
        //update trending topics start cycle
        topicsUpdater.updateTrendingTopics();
    }

}
