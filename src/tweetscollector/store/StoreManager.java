/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tweetscollector.store;

import java.io.File;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import tweetscollector.Configs;
import tweetscollector.TweetToStore;
import twitter4j.Status;

/**
 *
 * This class is the wrapper for the store of collected tweets
 *
 * @author intelWorX
 *
 */
public class StoreManager extends ConcurrentHashMap<String, StoreEntry> {

    final private static StoreManager instance = new StoreManager();
    private final File storageDir;
    private final HashSet<String> clearedTopics = new HashSet<String>();

    private StoreManager() {
        super();
        storageDir = new File(Configs.OUTPUT_DIR);
        if (!storageDir.exists() || !storageDir.isDirectory() || !storageDir.canWrite()) {
            if (!storageDir.mkdirs()) {
                throw new RuntimeException("The output directory is not accessible.");
            }
        }
    }

    public static StoreManager getInstance() {
        return instance;
    }

    public StoreEntry addTopic(String topic) {
        if (!clearedTopics.contains(topic) && get(topic) == null) {
            put(topic, new StoreEntry(topic));
        }
        return get(topic);
    }

    public void addTweet(Status tweet) {
        TweetToStore tweetToStore = null;
        //System.out.println("Finding Topics that match: " + tweet.getId() + ": " + tweet.getText());
        String testText = tweet.getText().toLowerCase();
        for (StoreEntry entry : values()) {
            //System.out.println("\tTesting: " + entry.pattern);
            if (testText.contains(entry.pattern)) {
                if (tweetToStore == null) {
                    System.out.println("\tFound match: "+entry.pattern);
                    //no multiple creation, create on demand
                    tweetToStore = new TweetToStore(tweet);
                }

                entry.addTweet(tweetToStore);
            }
        }
    }

    public Set<String> getTrendingTopics() {
        return keySet();
    }

    /**
     * Cleans the store of over due topics
     *
     * @return
     */
    public int cleanUp() {
        int cleaned = 0;
        Iterator<String> keyIterator = keySet().iterator();

        String topic;
        StoreEntry entry;

        while (keyIterator.hasNext()) {
            topic = keyIterator.next();
            entry = get(topic);
            if (entry.shouldStop()) {
                entry.saveToDisk(storageDir); //save to disk
                entry.clear(); //clear the entry
                keyIterator.remove();//remove key
                remove(topic); //remove from collectio
                cleaned++;
                clearedTopics.add(topic);
                System.out.println("Removing topic: " + topic);
            }
        }

        return cleaned;
    }

    public void saveAll() {
        if (storageDir.exists() && storageDir.canWrite()) {
            for (StoreEntry entry : values()) {
                entry.saveToDisk(storageDir);
                entry.clear();
            }
        }
    }

}
