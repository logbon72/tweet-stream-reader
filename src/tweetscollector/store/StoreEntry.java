/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tweetscollector.store;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Formatter;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPOutputStream;
import tweetscollector.Configs;
import tweetscollector.TweetToStore;

/**
 *
 * @author intelWorX
 */
public class StoreEntry extends ConcurrentHashMap<Long, TweetToStore> {

    protected long startedSince;
    protected String trendingTopic;
    protected String pattern;

    public StoreEntry(String trendingTopic) {
        super();
        startedSince = System.currentTimeMillis();
        this.trendingTopic = trendingTopic;
        //pattern = Pattern.compile(trendingTopic, Pattern.CASE_INSENSITIVE);
        pattern = trendingTopic.replaceAll("#", "").toLowerCase();
    }

    public long getStartedSince() {
        return startedSince;
    }

    public String getPattern() {
        return pattern;
    }

    public void saveToDisk(File outputDir) {
        if (!isEmpty()) {
            String outputName = trendingTopic.replaceAll("[^A-Z0-9a-z_]+", "") + "_"
                    + System.currentTimeMillis() + ".json.gz";
            File outputFile = new File(outputDir, outputName);
            try {
                FileOutputStream fos = new FileOutputStream(outputFile);
                GZIPOutputStream gzipos = new GZIPOutputStream(fos);
                Formatter writer = new Formatter(gzipos);
                for (TweetToStore tweetToStore : values()) {
                    writer.format("%s%n", tweetToStore.getJSONString());
                }
                writer.flush();
                writer.close();
                gzipos.close();
                fos.close();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(StoreEntry.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(StoreEntry.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    public void addTweet(TweetToStore tweet) {
        //urls
        put(tweet.getId(), tweet);
    }

    public boolean shouldStop() {
        return (System.currentTimeMillis() - startedSince) >= Configs.TRENDS_PERIOD || size() > Configs.TRENDS_LIMIT;
    }
}
