/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tweetscollector;

import tweetscollector.store.StoreEntry;
import tweetscollector.store.StoreManager;
import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;

/**
 *
 * @author intelWorX
 */
public class TweetReceiver implements StatusListener {

    private long lastTrendsUpdate = 0;
    private long lastCleanUp = 0;
    private static final StoreManager storeManager = StoreManager.getInstance();
    private final TrendUpdater trendUpdater;
    private final StoreCleaner storeCleaner;

    public TweetReceiver() {
        trendUpdater = new TrendUpdater();
        storeCleaner = new StoreCleaner();
        lastTrendsUpdate = lastCleanUp = System.currentTimeMillis();
    }

    @Override
    public void onStatus(Status status) {
        storeManager.addTweet(status);
        long currentSysTime = System.currentTimeMillis();

        //add status then check if we need to do cleanup
        if ((currentSysTime - lastTrendsUpdate) > Configs.TRENDS_UPDATE_INTERVAL) {
            Thread updater = new Thread(trendUpdater);
            updater.start();
            lastTrendsUpdate = currentSysTime;
            System.out.println("Updating trends... setting last update to: "+lastTrendsUpdate);
        }

        if ((currentSysTime - lastCleanUp) > Configs.CLEAN_INTERVAL) {
            Thread cleaner = new Thread(storeCleaner);
            cleaner.start();
            lastCleanUp = currentSysTime;
            System.out.println("Cleaning trends... setting last clean to: "+lastTrendsUpdate);
        }

    }

    @Override
    public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {
        for (StoreEntry storeEntry : storeManager.values()) {
            if (storeEntry.get(statusDeletionNotice.getStatusId()) != null) {
                storeEntry.remove(statusDeletionNotice.getStatusId());
            }
        }
    }

    @Override
    public void onTrackLimitationNotice(int numberOfLimitedStatuses) {
        System.out.println("Track Limitation Notice: " + numberOfLimitedStatuses);
        storeManager.cleanUp();
    }

    @Override
    public void onScrubGeo(long userId, long upToStatusId) {
        System.out.println("Not tracking Geo!");
    }

    @Override
    public void onStallWarning(StallWarning warning) {
        System.out.println("Stall warning received: " + warning.getMessage());
    }

    @Override
    public void onException(Exception ex) {
        System.out.println("Exception Occured: " + ex.getMessage());
    }

    public static final class TrendUpdater implements Runnable {

        @Override
        public void run() {
            TrendingTopicsUpdater.getInstance().updateTrendingTopics();
        }
    }

    public static final class StoreCleaner implements Runnable {

        @Override
        public void run() {
            StoreManager.getInstance().cleanUp();
        }

    }
}
