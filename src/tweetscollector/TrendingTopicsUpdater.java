/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tweetscollector;

import java.util.ArrayList;
import java.util.List;
import tweetscollector.store.StoreManager;
import tweetscollector.trends.TrendsUpdateListener;
import twitter4j.Trend;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;

/**
 *
 * @author intelWorX
 */
final public class TrendingTopicsUpdater {

    protected Twitter twitter;
    protected Trend[] trendingTopics;
    final private static TrendingTopicsUpdater instance = new TrendingTopicsUpdater();

    protected List<TrendsUpdateListener> trendUpdateListeners;

    private TrendingTopicsUpdater() {
        twitter = new TwitterFactory().getInstance();
        trendUpdateListeners = new ArrayList<TrendsUpdateListener>();
    }

    public static TrendingTopicsUpdater getInstance() {
        return instance;
    }

    public void updateTrendingTopics() {
        try {
            trendingTopics = twitter.getPlaceTrends(Configs.LOCATION_WOEID).getTrends();

            for (Trend trend : trendingTopics) {
                StoreManager.getInstance().addTopic(trend.getName());
            }
//            String [] topics = {"beth", "Yankees", "#BaseballIsBack"};
//            for (String topic : topics) {
//                StoreManager.getInstance().addTopic(topic);
//            }

            for (TrendsUpdateListener listener : trendUpdateListeners) {
                listener.trendsUpdated();
            }
        } catch (TwitterException ex) {
            System.out.println("Could not update trending topics list... " + ex.getMessage());
        }
        
    }

    public void addUpdateListener(TrendsUpdateListener updateListener) {
        if (!trendUpdateListeners.contains(updateListener)) {
            trendUpdateListeners.add(updateListener);
        }
    }
}
